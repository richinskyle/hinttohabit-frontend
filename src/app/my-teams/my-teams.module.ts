import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../shared';

import { MyTeamsRoutingModule } from './my-teams-routing.module';
import { MyTeamsComponent } from './my-teams.component';
import { FormsModule } from '@angular/forms';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { CKEditorModule } from 'ng2-ckeditor';
import { QuillModule } from 'ngx-quill'

@NgModule({
  imports: [
    CommonModule,
    MyTeamsRoutingModule,
    PageHeaderModule,
    FormsModule,
    CKEditorModule,
    QuillModule,
    NgxSmartModalModule.forChild(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
  ],
  declarations: [MyTeamsComponent],
  providers: [NgxSmartModalService]
})
export class MyTeamsModule { }
