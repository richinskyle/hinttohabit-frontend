import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HintToHabitApi } from '../shared/services/hinttohabit-api.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { AuthServiceProvider } from '../shared/services/auth-service';

import swal from 'sweetalert2';
import * as _ from 'lodash';
import * as $ from 'jquery';
declare var Quill:any;

@Component({
  selector: 'app-my-teams',
  templateUrl: './my-teams.component.html',
  styleUrls: ['./my-teams.component.scss']
})
export class MyTeamsComponent implements OnInit {

	displayId = this.auth.currentUser.info.id;
    private sub: any;
	myTeams: any;
    week1Tab = true;
    week2Tab = false;
    week3Tab = false;
    week4Tab = false;
    week5Tab = false;
    week6Tab = false;
    week7Tab = false;
    week8Tab = false;

    emailTeamVar: any;
    emailTeamContent: any = {team: null, email: null};

    introEmailContent = "<p>Hello!</p><p>This is the email address that you will send your weekly numbers into each week.</p><p><strong>How the challenge will work: </strong></p><p>There will be two winners on your team- the person who has the highest percentage of weight loss (just like they do on the Biggest Loser TV show) and the person who earns the most points. Each of these winners will receive a $30 Amazon gift card. Because we want as many winners as possible, we only allow one prize per person (so if the person with the highest percentage of weight loss also has the most points, we will award one of the prizes to second place).</p><p>As a team, you are also competing against all the other teams (12 total). The team who earns the highest average amount of points at the end of the 8 weeks will receive a $10 Amazon gift card for each person on their team.</p><p>As part of your daily healthy habits, your job is to make contact with at least one member of your team. You can do this via email, text, phone call, Facebook message, or whatever is most convenient for you. You aren't just doing this challenge for yourself - you are part of a team that needs you!</p><p>We would also love to have you join our private Facebook group - click this link <a href='https://www.facebook.com/groups/402717666842582/'>(https://www.facebook.com/groups/402717666842582/)</a> and request to join and we will get you added in. In the group, feel free to introduce yourself! This Facebook group is a safe place to share recipes, workout ideas, motivational quotes, etc. It's also a place to get help and support if you need it.</p><p>Please let us know if you have any questions! We can't wait to get started!</p><p>Let's do this!</p><p>-Laura & Erika</p>";

    lodash = _;
    sendingEmails = false;
    reminderEmailContent = "<p>Remember that Sunday's points count towards your weekly total, so be sure to include them!</p><p>Please send the following to hinttohabit@gmail.com by MONDAY at 9 am (MST):</p><ul><li>Name:</li><li>Team #:</li><li>New current weight:</li><li>Total points earned this week:</li></ul><p>Also, please attach a picture of your weight.</p>";

  	constructor(
  		private route: ActivatedRoute,
  		public api: HintToHabitApi,
        public ngxSmartModalService: NgxSmartModalService,
        public auth: AuthServiceProvider) { }

  	ngOnInit() {
        localStorage.setItem('last_state', '/my-teams/' + this.auth.currentUser.info.id);

  		this.sub = this.route.params.subscribe(params => {
	  		this.api.getMyTeams(+params['id']).then(data => {
		        this.myTeams = data;
		    });
	    });
  	}

    saveData(){
        this.api.updateMyTeamsData(this.myTeams).then(data => {
            this.myTeams = data;
            swal('Saved!', '', 'success');
        });
    }

    sendReminderEmail(){
        this.sendingEmails = true;
        this.api.reminderEmail(this.reminderEmailContent, this.myTeams).then(data => {
            $('#reminderModal').click();
            swal('Emails Sent!', data.toString(), 'success');
            this.sendingEmails = false;
        });
    }

    disableMember(participantId, type){
        this.api.disableMember(participantId, this.displayId, type).then(data => {
            this.myTeams = data;
        });
    }

    setTeamVariable(team){
        this.emailTeamContent.team = team;
        this.emailTeamVar = team.name;
    }

    changeTeamDisplay(){
        this.api.getMyTeams(this.displayId).then(data => {
            this.myTeams = data;
        });
    }

    sendTeamEmail(){
        this.api.sendTeamEmail(this.emailTeamContent).then(data => {
            var successfulEmails = data['successfulEmails'];
            var failedEmailAddresses = data['failedEmails'];
            if(_.size(failedEmailAddresses) == 0){
                failedEmailAddresses = 0;
            }
            var message = successfulEmails + ' emails successfully sent!';
            swal(message, '', 'success');
            this.emailTeamContent = {team: null, email: null};
        });
    }

    emailResults(){
        swal({
          title: 'Send results for which week?',
          input: 'select',
          inputOptions: {
            '1': 'Week 1',
            '2': 'Week 2',
            '3': 'Week 3',
            '4': 'Week 4',
            '5': 'Week 5',
            '6': 'Week 6',
            '7': 'Week 7',
            '8': 'Week 8'
          },
          inputPlaceholder: 'Select week',
          showCancelButton: true,
          confirmButtonColor: '#13AADD',
          cancelButtonColor: '#f58a07',
          confirmButtonText: 'Next'
        }).then((week) => {
            if (week.value) {
                swal({
                  title: 'Send your teams a custom message!',
                  input: 'textarea',
                  inputPlaceholder: 'This is optional',
                  showCancelButton: true,
                  confirmButtonText: 'Send!',
                  confirmButtonColor: '#13AADD',
                  cancelButtonColor: '#f58a07',
                  showLoaderOnConfirm: true,
                  preConfirm: (message) => {
                    return new Promise((resolve) => {
                        this.api.emailResults(this.myTeams, week.value, message, this.displayId).then(data => {
                            if(data == "Success"){
                                resolve();
                            }else{
                                swal('Oops', data.toString(), 'error');
                            }
                        });
                    })
                  },
                  allowOutsideClick: () => !swal.isLoading()
                }).then((message) => {
                    if(message.dismiss){
                        //
                    }else{
                        swal('Results Sent!', '', 'success');
                    }
                })
            }
        })
    }

    introEmail(){
        this.api.introEmail(this.myTeams, this.introEmailContent).then(data => {
            swal('Emails Sent!', data.toString(), 'success');
        });
    }

    memberDropout(participant){
        swal({
          title: 'Confirm Dropout?',
          text: "This will remove selected participant from the team. All participant data will be available if needed.",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Confirm'
        }).then((result) => {
          if (result.value) {
            this.api.memberDropout(participant, this.displayId).then(data => {
                this.myTeams = data;
            });
          }
        })
    }

    calcTotWeightChange(initWeight, currWeight, type){
        if(type == 'lbs'){
            return (currWeight - initWeight).toFixed(1);
        }else if(type == '%'){
            return (((1.0 - (currWeight / initWeight)) * 100) * -1).toFixed(2);
        }
    }

    calcTotPoints(results, week){
        if(week == 'week2'){
            return (+results.week_1_points + +results.week_2_points);
        }else if(week == 'week3'){
            return (+results.week_1_points + +results.week_2_points + +results.week_3_points);
        }else if(week == 'week4'){
            return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points);
        }else if(week == 'week5'){
            return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points + +results.week_5_points);
        }else if(week == 'week6'){
            return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points + +results.week_5_points + +results.week_6_points);
        }else if(week == 'week7'){
            return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points + +results.week_5_points + +results.week_6_points + +results.week_7_points);
        }else if(week == 'week8'){
            return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points + +results.week_5_points + +results.week_6_points + +results.week_7_points + +results.week_8_points);
        }
    }

    toggleTab(tabNum){
        if(tabNum == 1){
            this.week1Tab = true;
            this.week2Tab = false;
            this.week3Tab = false;
            this.week4Tab = false;
            this.week5Tab = false;
            this.week6Tab = false;
            this.week7Tab = false;
            this.week8Tab = false;
        }else if(tabNum == 2){
            this.week2Tab = true;
            this.week1Tab = false;
            this.week3Tab = false;
            this.week4Tab = false;
            this.week5Tab = false;
            this.week6Tab = false;
            this.week7Tab = false;
            this.week8Tab = false;
        }else if(tabNum == 3){
            this.week3Tab = true;
            this.week1Tab = false;
            this.week2Tab = false;
            this.week4Tab = false;
            this.week5Tab = false;
            this.week6Tab = false;
            this.week7Tab = false;
            this.week8Tab = false;
        }else if(tabNum == 4){
            this.week4Tab = true;
            this.week1Tab = false;
            this.week2Tab = false;
            this.week3Tab = false;
            this.week5Tab = false;
            this.week6Tab = false;
            this.week7Tab = false;
            this.week8Tab = false;
        }else if(tabNum == 5){
            this.week5Tab = true;
            this.week1Tab = false;
            this.week2Tab = false;
            this.week3Tab = false;
            this.week4Tab = false;
            this.week6Tab = false;
            this.week7Tab = false;
            this.week8Tab = false;
        }else if(tabNum == 6){
            this.week6Tab = true;
            this.week1Tab = false;
            this.week2Tab = false;
            this.week3Tab = false;
            this.week4Tab = false;
            this.week5Tab = false;
            this.week7Tab = false;
            this.week8Tab = false;
        }else if(tabNum == 7){
            this.week7Tab = true;
            this.week1Tab = false;
            this.week2Tab = false;
            this.week3Tab = false;
            this.week4Tab = false;
            this.week5Tab = false;
            this.week6Tab = false;
            this.week8Tab = false;
        }else if(tabNum == 8){
            this.week8Tab = true;
            this.week1Tab = false;
            this.week2Tab = false;
            this.week3Tab = false;
            this.week4Tab = false;
            this.week5Tab = false;
            this.week6Tab = false;
            this.week7Tab = false;
        }
    }

}
