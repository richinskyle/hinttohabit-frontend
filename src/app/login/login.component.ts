import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { HintToHabitApi } from '../shared/services/hinttohabit-api.service';
import { AuthServiceProvider } from '../shared/services/auth-service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

	credentials: any = {};
    error = '';

    constructor(public router: Router, public api: HintToHabitApi, public auth: AuthServiceProvider) {}

    ngOnInit() {}

    onLoggedin() {
        this.auth.login(this.credentials).subscribe(allowed => {
            if (allowed) {     
                this.router.navigate(['/dashboard']);
            } else {
                this.error = 'Invalid credentials';
            }
        },error => {
            this.error = 'Something went wrong! Try again or contact the system administrator';
        });
    }
}
