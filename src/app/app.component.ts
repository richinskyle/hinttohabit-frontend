import { Component, OnInit } from '@angular/core';
import { AuthServiceProvider } from './shared/services/auth-service';
import { HintToHabitApi } from './shared/services/hinttohabit-api.service';
import { Router } from '@angular/router';

import * as _ from 'lodash';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    constructor(public auth: AuthServiceProvider, public api: HintToHabitApi, public router: Router) {

    	if(_.isEmpty(auth.currentUser.info)){
            if(localStorage.getItem('remembered_user') != null){
                auth.currentUser.info = JSON.parse(localStorage.getItem('remembered_user'));
            }else{
                var token = localStorage.getItem('api_token');
                if(!token){
                    this.logout();
                }else{
                    this.api.getCurrentUser(token).then(data => {
                        if(!data['id']){
                            this.logout();
                        }else{
                            auth.currentUser.info = data;
                            localStorage.setItem('remembered_user', JSON.stringify(auth.currentUser.info));
                            if(localStorage.getItem('last_state')){
                                router.navigate([localStorage.getItem('last_state')]);
                            }else{
                                router.navigate(['/dashboard']);
                            }
                        }
                    }).catch(error => {
                        if(error){
                            this.logout();
                        }
                    });
                }
            }
        }
    }

    ngOnInit() {
    }

    logout(){
    	this.auth.currentUser.info = null;
    	localStorage.clear();
    	this.router.navigate(['/login']);
    }
}
