import { Component, OnInit } from '@angular/core';
import { HintToHabitApi } from '../shared/services/hinttohabit-api.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {

	results: any;
    lodash = _;
    resultsType = 'points';
    resultsWeek = 1;
    indvTab = true;
    teamsTab = false;

  	constructor(public api: HintToHabitApi) {

  	}

  	ngOnInit() {

  		this.api.getLeaderboard(this.resultsType, this.resultsWeek).then(data => {
            this.results = _.toArray(data);
            console.log(this.results);
	    });

  	}

    changeResultsType(){
        this.api.getLeaderboard(this.resultsType, this.resultsWeek).then(data => {
            this.results = _.toArray(data);
            console.log(this.results);
        });
    }

    calcAverage(resultsData, length, type){
        let total = 0;
        for(var i = 0; i < _.size(resultsData); i++){
            if(length == 'week' && type == 'points'){
                total += resultsData[i].week_points;
            }else if(length == 'total' && type == 'points'){
                total += resultsData[i].total_points;
            }else if(length == 'week' && type == 'weight'){
                total += resultsData[i].week_weight_loss;
            }else if(length == 'total' && type == 'weight'){
                total += resultsData[i].total_weight_loss;
            }

        }
        if(type == 'points'){
            return (total / _.size(resultsData)).toFixed(1);
        }else if(type =='weight'){
            return (total / _.size(resultsData)).toFixed(2);
        }

    }

    toggleTab(tab){
        if(tab == 'indv'){
            this.indvTab = true;
            this.teamsTab = false;
        }else if(tab == 'teams'){
            this.teamsTab = true;
            this.indvTab = false;
        }
    }

}
