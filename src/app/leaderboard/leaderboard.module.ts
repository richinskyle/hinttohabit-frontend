import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../shared';
import { FormsModule } from '@angular/forms';

import { LeaderboardRoutingModule } from './leaderboard-routing.module';
import { LeaderboardComponent } from './leaderboard.component';

@NgModule({
  imports: [
    CommonModule,
    PageHeaderModule,
    LeaderboardRoutingModule,
    FormsModule
  ],
  declarations: [LeaderboardComponent]
})
export class LeaderboardModule { }
