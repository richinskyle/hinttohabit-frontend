import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { HttpClient } from '@angular/common/http';
import { HintToHabitApi } from '../../shared/services/hinttohabit-api.service';

import * as _ from 'lodash';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    participants: any;
    lodash = _;

    constructor(
        private http: HttpClient,
        private api: HintToHabitApi) {
    }

    ngOnInit() {
        localStorage.setItem('last_state', '/dashboard');
        this.api.loadParticipants().then(data => {
            this.participants = data;
        })

    }

    strReplace(s){
          var s2 = (""+s).replace(/\D/g, '');
          var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
          return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        //return number.replace(/[^a-zA-Z0-9]/g,'');
    }

    refreshList() {
        this.api.refreshList().then(data => {
            this.participants = data;
        })
    }
}
