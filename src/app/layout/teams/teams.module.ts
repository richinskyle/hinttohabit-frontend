import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamsRoutingModule } from './teams-routing.module';
import { TeamsComponent } from './teams.component';
import { PageHeaderModule } from './../../shared';
import { FormsModule } from '@angular/forms';
import { Ng2DragDropModule } from 'ng2-drag-drop';

@NgModule({
    imports: [CommonModule, TeamsRoutingModule, PageHeaderModule, FormsModule, Ng2DragDropModule.forRoot()],
    declarations: [TeamsComponent]
})
export class TeamsModule {}
