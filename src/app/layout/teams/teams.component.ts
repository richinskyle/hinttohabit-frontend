import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { HintToHabitApi } from '../../shared/services/hinttohabit-api.service';

import * as _ from 'lodash';
import swal from 'sweetalert2';

@Component({
    selector: 'app-teams',
    templateUrl: './teams.component.html',
    styleUrls: ['./teams.component.scss'],
    animations: [routerTransition()]
})
export class TeamsComponent implements OnInit {

	teams: any;
	participants: any;
	addRowTeamId;
	addingRow = false;
	selectedParticipant;
	newMember;

	lodash = _;

    constructor(
    	private api: HintToHabitApi) {

    	this.getTeams();
    	this.getParticipants();
    }

    ngOnInit() {
    	localStorage.setItem('last_state', '/teams');
    }

    getTeams(){
    	this.api.getTeams().then(data => {
	        this.teams = data;
	    });
    }

    getParticipants(){
    	this.api.getParticipants().then(data => {
	    	this.participants = data;
	    });
    }

    createTeam(){
    	swal({
		  title: 'Team Name',
		  input: 'text',
		  inputPlaceholder: 'Enter the team name',
		  showCancelButton: true,
		  confirmButtonText: 'Submit',
		  allowOutsideClick: () => !swal.isLoading()
		}).then((result) => {
		  	if (result.value) {
		    	this.api.newTeam({teamName: result.value}).then(data => {
		    		if(data == 'Team exists'){
		    			swal('Team Exists!', 'This team already exists', 'warning');
		    		}else{
		    			this.teams = data;
		    		}
		    	});
		  	}
		})
    }

    deleteTeam(team){
    	swal({
		  	title: 'Are you sure?',
		  	text: "All associated team members will be unassigned to a team",
		  	type: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Delete!'
		}).then((result) => {
		  	if (result.value) {
		    	this.api.deleteTeam(team).then(data => {
		    		this.teams = data;
		    		this.getParticipants();
		    		swal('', '', 'success');
		    	});
		  	}
		})
    }

    onItemDrop(e: any, team) {
        // Get the dropped data here
        if(e.dragData.participant.assigned_team == team.name){
        	// do nothing
        }else{
        	this.submitTeamMember(team.id, e.dragData.participant.id);
        }
    }

    addTeamMember(team){
    	this.addingRow = true;
    	this.addRowTeamId = team;
    }

    submitTeamMember(teamId, newMemberId){
		swal({
		  	title: 'Confirm Switch?',
		  	text: "",
		  	type: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Confirm!'
		}).then((result) => {
		  	if (result.value) {
		    	this.api.newTeamMember({teamId: teamId, participantId: newMemberId}).then(data => {
		    		this.teams = data;
		    		this.getParticipants();
		    		this.addingRow = false;
		    		this.newMember = null;
		    	});
		  	}
		})
    }

    changeTeamCoach(team){
    	this.api.changeTeamCoach(team).then(data => {
    		this.teams = data;
    		swal('', '', 'success');
    	});
    }

    randomizeTeams(){
    	swal({
		  title: 'How many groups?',
		  text: 'This will erase any pre-existing teams',
		  input: 'select',
		  inputOptions: {
            '1': '1',
            '2': '2',
          },
          inputPlaceholder: 'Number of groups',
          showCancelButton: true,
          confirmButtonColor: '#13AADD',
          cancelButtonColor: '#f58a07',
          confirmButtonText: 'Create',
          showLoaderOnConfirm: true,
		  preConfirm: (groups) => {
		    return new Promise((resolve) => {
		    	if(groups){
		    		this.api.randomizeTeams(groups).then(data => {
			    		this.teams = data;
			    		this.getParticipants();
			    		resolve();
			    	});
		    	}else{
		    		swal.showValidationError('You must select a number');
		    		swal.hideLoading();
		    	}
		    })
		  },
		  allowOutsideClick: () => !swal.isLoading()
		}).then((result) => {
		  if (result.value) {
		    swal('', '', 'success');
		  }
		})
    }

    strReplace(s){
          var s2 = (""+s).replace(/\D/g, '');
          var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
          return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        //return number.replace(/[^a-zA-Z0-9]/g,'');
    }
}
