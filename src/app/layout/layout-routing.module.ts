import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { ParticipantModule } from '../participant/participant.module';
import { MyTeamsModule } from '../my-teams/my-teams.module';
import { TeamTrackerModule } from '../team-tracker/team-tracker.module';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'teams', loadChildren: './teams/teams.module#TeamsModule' },
            { path: 'my-teams/:id', loadChildren: '../my-teams/my-teams.module#MyTeamsModule' },
            { path: 'participant/:id', loadChildren: '../participant/participant.module#ParticipantModule' },
            { path: 'team-tracker/:id', loadChildren: '../team-tracker/team-tracker.module#TeamTrackerModule' },
            { path: 'leaderboard', loadChildren: '../leaderboard/leaderboard.module#LeaderboardModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
