import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HintToHabitApi } from '../shared/services/hinttohabit-api.service';

import swal from 'sweetalert2';

import * as _ from 'lodash';

@Component({
  selector: 'app-team-tracker',
  templateUrl: './team-tracker.component.html',
  styleUrls: ['./team-tracker.component.scss']
})
export class TeamTrackerComponent implements OnInit {

	private sub: any;
	lodash = _;
	teamData: any;
	id: number;
	week1Tab = true;
	week2Tab = false;
	week3Tab = false;
	week4Tab = false;
	week5Tab = false;
	week6Tab = false;
	week7Tab = false;
	week8Tab = false;

  	constructor(
  		private route: ActivatedRoute,
  		public api: HintToHabitApi) {

  	}

  	ngOnInit() {

  		this.sub = this.route.params.subscribe(params => {
  			this.id = +params['id'];
	  		this.api.loadTeam(+params['id']).then(data => {
		        this.teamData = data;
                localStorage.setItem('last_state', '/team-tracker/' + this.id);
		    });
	    });

  	}

  	saveData(){
  		this.api.updateTeamData(this.teamData).then(data => {
	        this.teamData = data;
	        swal('Saved!', '', 'success');
	    });
  	}

  	calcTotWeightChange(initWeight, currWeight, type){
  		if(type == 'lbs'){
  			return (currWeight - initWeight);
  		}else if(type == '%'){
  			return (((1.0 - (currWeight / initWeight)) * 100) * -1).toFixed(2);
  		}
  	}

  	calcTotPoints(results, week){
  		if(week == 'week2'){
  			return (+results.week_1_points + +results.week_2_points);
  		}else if(week == 'week3'){
  			return (+results.week_1_points + +results.week_2_points + +results.week_3_points);
  		}else if(week == 'week4'){
  			return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points);
  		}else if(week == 'week5'){
  			return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points + +results.week_5_points);
  		}else if(week == 'week6'){
  			return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points + +results.week_5_points + +results.week_6_points);
  		}else if(week == 'week7'){
  			return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points + +results.week_5_points + +results.week_6_points + +results.week_7_points);
  		}else if(week == 'week8'){
  			return (+results.week_1_points + +results.week_2_points + +results.week_3_points + +results.week_4_points + +results.week_5_points + +results.week_6_points + +results.week_7_points + +results.week_8_points);
  		}
  	}

  	toggleTab(tabNum){
  		if(tabNum == 1){
  			this.week1Tab = true;
  			this.week2Tab = false;
  			this.week3Tab = false;
  			this.week4Tab = false;
  			this.week5Tab = false;
  			this.week6Tab = false;
  			this.week7Tab = false;
  			this.week8Tab = false;
  		}else if(tabNum == 2){
  			this.week2Tab = true;
  			this.week1Tab = false;
  			this.week3Tab = false;
  			this.week4Tab = false;
  			this.week5Tab = false;
  			this.week6Tab = false;
  			this.week7Tab = false;
  			this.week8Tab = false;
  		}else if(tabNum == 3){
  			this.week3Tab = true;
  			this.week1Tab = false;
  			this.week2Tab = false;
  			this.week4Tab = false;
  			this.week5Tab = false;
  			this.week6Tab = false;
  			this.week7Tab = false;
  			this.week8Tab = false;
  		}else if(tabNum == 4){
  			this.week4Tab = true;
  			this.week1Tab = false;
  			this.week2Tab = false;
  			this.week3Tab = false;
  			this.week5Tab = false;
  			this.week6Tab = false;
  			this.week7Tab = false;
  			this.week8Tab = false;
  		}else if(tabNum == 5){
  			this.week5Tab = true;
  			this.week1Tab = false;
  			this.week2Tab = false;
  			this.week3Tab = false;
  			this.week4Tab = false;
  			this.week6Tab = false;
  			this.week7Tab = false;
  			this.week8Tab = false;
  		}else if(tabNum == 6){
  			this.week6Tab = true;
  			this.week1Tab = false;
  			this.week2Tab = false;
  			this.week3Tab = false;
  			this.week4Tab = false;
  			this.week5Tab = false;
  			this.week7Tab = false;
  			this.week8Tab = false;
  		}else if(tabNum == 7){
  			this.week7Tab = true;
  			this.week1Tab = false;
  			this.week2Tab = false;
  			this.week3Tab = false;
  			this.week4Tab = false;
  			this.week5Tab = false;
  			this.week6Tab = false;
  			this.week8Tab = false;
  		}else if(tabNum == 8){
  			this.week8Tab = true;
  			this.week1Tab = false;
  			this.week2Tab = false;
  			this.week3Tab = false;
  			this.week4Tab = false;
  			this.week5Tab = false;
  			this.week6Tab = false;
  			this.week7Tab = false;
  		}
  	}

}
