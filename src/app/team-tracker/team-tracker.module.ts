import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamTrackerRoutingModule } from './team-tracker-routing.module';
import { TeamTrackerComponent } from './team-tracker.component';
import { PageHeaderModule } from './../shared';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TeamTrackerRoutingModule,
    PageHeaderModule,
    FormsModule
  ],
  declarations: [TeamTrackerComponent]
})
export class TeamTrackerModule { }
