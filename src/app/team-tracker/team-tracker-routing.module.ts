import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamTrackerComponent } from './team-tracker.component';

const routes: Routes = [
    {
        path: '', component: TeamTrackerComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamTrackerRoutingModule { }
