import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs';

@Injectable()
export class HintToHabitApi {
	
	public baseUrl = '';

	constructor(public http: Http){
		if(location.hostname == 'portal.hinttohabit.com'){
			this.baseUrl = 'https://api.hinttohabit.com';
		}else{
			this.baseUrl = 'http://api.hinttohabit.test';
		}
	}

	login(credentials){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/login', credentials)
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	getCurrentUser(api_token){
		let headers = new Headers();
		headers.set('Authorization', api_token);
		let options = new RequestOptions({ headers: headers });
		return new Promise((resolve, reject) => {
			this.http.get(this.baseUrl + '/current-user', options)
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		});
	}

	getTeams(){
		return new Promise(resolve => {
			this.http.get(this.baseUrl + '/teams')
				.subscribe(res => resolve(res.json()));
		})
	}

	getMyTeams(id){
		return new Promise(resolve => {
			this.http.get(this.baseUrl + '/my-teams/' + id)
				.subscribe(res => resolve(res.json()));
		})
	}

	getParticipants(){
		return new Promise(resolve => {
			this.http.get(this.baseUrl + '/participants')
				.subscribe(res => resolve(res.json()));
		})
	}

	loadParticipants(){
		return new Promise(resolve => {
			this.http.get(this.baseUrl + '/participants')
				.subscribe(res => resolve(res.json()));
		})
	}

	loadParticipant(id){
		return new Promise(resolve => {
			this.http.get(this.baseUrl + '/participant/' + id)
				.subscribe(res => resolve(res.json()));
		})
	}

	loadTeam(id){
		return new Promise(resolve => {
			this.http.get(this.baseUrl + '/team/' + id)
				.subscribe(res => resolve(res.json()));
		})
	}

	getLeaderboard(type, week){
		return new Promise(resolve => {
			this.http.get(this.baseUrl + '/leaderboard/' + type + '/week/' + week)
				.subscribe(res => resolve(res.json()));
		})
	}

	memberDropout(participantId, coach){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/member-dropout/' + participantId, {coachId: coach})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	disableMember(participantId, coach, type){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/disable-participant/' + participantId, {coachId: coach, type: type})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	newTeam(object){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/new-team', object)
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	newTeamMember(object){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/new-team-member', object)
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	randomizeTeams(groups){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/randomize-teams', {numberOfGroups: groups})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	updateTeamData(teamData){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/update-team-data/' + teamData.id, {data: teamData})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	updateMyTeamsData(myTeams){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/update-my-teams-data/', {data: myTeams})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	changeTeamCoach(team){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/change-team-coach/' + team.id, {teamData: team})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	deleteTeam(team){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/delete-team/' + team.id, {})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	sendTeamEmail(emailContent){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/send-team-email/' + emailContent.team.id, {emailContent: emailContent})
				.subscribe(
					res => resolve(res.json()),
					err => {
						alert(err._body);
						reject(err);
					}
				);
		})
	}

	emailResults(teams, week, message, coachId){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/email-results', {teams: teams, week: week, message: message, coachId: coachId})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	introEmail(teams, email){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/intro-email', {teams: teams, email: email})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	reminderEmail(emailContent, teams){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/reminder-email', {emailContent: emailContent, teams: teams})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}

	refreshList(){
		return new Promise((resolve, reject) => {
			this.http.post(this.baseUrl + '/refresh-list', {})
				.subscribe(
					res => resolve(res.json()),
					err => {
						reject(err);
					}
				);
		})
	}
}