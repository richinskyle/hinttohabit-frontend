import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { HintToHabitApi } from './hinttohabit-api.service';
import * as _ from 'lodash';

export class User {
  	info: any = {};
  	api_token: any;
 
  	constructor(info: any = {}, api_token: any) {
    	this.info = info;
    	this.api_token = api_token;
  	}
}

@Injectable()
export class AuthServiceProvider {

    currentUser: User;

	constructor(
		public http: Http,
  		private api: HintToHabitApi) {

		this.currentUser = {
			info: {},
			api_token: '',
		};
	}
 
	public login(credentials) {
		if (credentials.username == null || credentials.password == null || credentials.username == '' || credentials.password == '') {
			return Observable.throw(false);
		} else {
			return Observable.create(observer => {
				this.api.login(credentials).then(authUser => {
					if(authUser != 'fail'){
						localStorage.setItem('api_token', authUser['api_token']);
                		this.currentUser = new User(authUser['user'], authUser['api_token']);
                		localStorage.setItem('remembered_user', JSON.stringify(this.currentUser.info));
						observer.next(true);
						observer.complete()
					}else{
						observer.next(false);
						observer.complete();
					}
				}).catch(error => {
					if(error.status == 401){
						observer.next(false);
						observer.complete();
					}
			    });
			});
		}
	}
 
	public getUserInfo() : User {
		return this.currentUser;
	}
 
	public logout() {
		return Observable.create(observer => {
			this.currentUser = null;
			observer.next(true);
			observer.complete();
		});
	}

}
