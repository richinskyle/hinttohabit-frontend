import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HintToHabitApi } from '../shared/services/hinttohabit-api.service';

@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.scss']
})
export class ParticipantComponent implements OnInit {

	private sub: any;
	participantData: any = {};

  	constructor(
  		private route: ActivatedRoute,
  		public api: HintToHabitApi) { 

  	}

  	ngOnInit() {

	  	this.sub = this.route.params.subscribe(params => {
	  		this.api.loadParticipant(+params['id']).then(data => {
		        this.participantData = data;
            localStorage.setItem('last_state', '/participant/' + +params['id']);
		    });
	    });

  	}

  	strReplace(s){
          var s2 = (""+s).replace(/\D/g, '');
		  var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
		  return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        //return number.replace(/[^a-zA-Z0-9]/g,'');
    }

}
